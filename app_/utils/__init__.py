from .endpoints import bind_routes

__all__ = [
    'bind_routes',
]
