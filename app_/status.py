from enum import Enum, unique


@unique
class SolutionStatus(str, Enum):
    SAVED = 'Saved'
    INIT = 'Init'
    WAITING = 'Waiting in queue'
    TESTING = 'Testing is in progress'
    NO_IMAGE = 'Testing image isn\'t found'
    PREPROCESSING_FAILED = 'Preprocessing failed'
    RUNNING_FAILED = 'Running failed'
    GRADING_FAILED = 'Grading failed'
    TESTING_COMPLETED = 'Testing completed'
