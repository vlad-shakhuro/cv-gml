from enum import Enum, unique


@unique
class SortOrder(str, Enum):
    DESC = 'desc'
    ASC = 'asc'


@unique
class SortBy(str, Enum):
    ID = 'id'
    DATE = 'date'
    MARK = 'mark'
    REVIEWS_COUNT = 'reviews_count'
