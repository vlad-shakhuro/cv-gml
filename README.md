# Installation

1. Install postgresql, redis, docker, python libs (requirements.txt)
2. Run `./app clean` to create pg database (it should allow password-less authentification from localhost)
3. Run `./app` to start web application..Navigate to http://127.0.0.1:5000/, use `test@example.com` and `12345` to login as admin.
4. Run `./worker` to start worker that checks programming tasks
