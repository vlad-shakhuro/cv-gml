import pytest
from flask import url_for


def test_index_no_login(client):
    response = client.get('/')
    assert response.status_code == 302
    assert response.headers.get('Location') == url_for('login')


@pytest.mark.usefixtures('user_logged_in')
def test_index_add_user_to_course(client):
    response = client.get('/')
    assert response.status_code == 302
    assert response.headers.get('Location') == url_for('add_user_to_course')
