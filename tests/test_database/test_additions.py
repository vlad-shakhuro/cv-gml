from flask_sqlalchemy import SQLAlchemy

from .database_setup_helper import DatabaseSetup


class TestDatabase(DatabaseSetup):
    def test_add_user(self, postgres: SQLAlchemy):
        from database import User

        user = self.add_user(postgres)

        query = postgres.session.query(User).filter(User.id == user.id).first()
        assert user in postgres.session, 'User not added to database'
        assert query is not None
        assert query.id == user.id
        assert query.email == user.email
        postgres.session.close()

    def test_add_task(self, postgres: SQLAlchemy):
        from database import Task

        user = self.add_user(postgres)
        task = self.add_task(user, postgres)

        query = postgres.session.query(Task).filter(Task.id == task.id).first()
        assert task in postgres.session, 'Task not added to database'
        assert query is not None
        assert query.id == task.id
        assert query.name == task.name
        postgres.session.close()

    def test_add_attached_task(self, postgres: SQLAlchemy):
        from database import AttachedTask

        user = self.add_user(postgres)
        task = self.add_task(user, postgres)
        attached_task = self.add_attached_task(task, postgres)

        query = (
            postgres.session.query(AttachedTask)
            .filter(AttachedTask.id == attached_task.id)
            .first()
        )
        assert (
            attached_task in postgres.session
        ), 'AttachedTask not added to database'
        assert query is not None
        assert query.id == attached_task.id
        assert query.task_id == attached_task.task_id
        postgres.session.close()

    def test_add_course(self, postgres: SQLAlchemy):
        from database import Course

        course = self.add_course(postgres)

        query = (
            postgres.session.query(Course)
            .filter(Course.id == course.id)
            .first()
        )
        assert course in postgres.session, 'Course not added to database'
        assert query is not None
        assert query.id == course.id
        assert query.name == course.name
        assert query.short_name == course.short_name
        postgres.session.close()

    def test_add_user_role(self, postgres: SQLAlchemy):
        from database import UserRole

        user = self.add_user(postgres)
        course = self.add_course(postgres)
        user_role = self.add_user_role(user, course, postgres)

        query = (
            postgres.session.query(UserRole)
            .filter(UserRole.id == user_role.id)
            .first()
        )
        assert user_role in postgres.session, 'UserRole not added to database'
        assert query is not None
        assert query.id == user_role.id
        assert query.user_id == user_role.user_id
        assert query.course_id == user_role.course_id
        postgres.session.close()

    def test_add_standing(self, postgres: SQLAlchemy):
        from database import Standings

        user = self.add_user(postgres)
        course = self.add_course(postgres)
        standing = self.add_standing(user, course, postgres)

        query = (
            postgres.session.query(Standings)
            .filter(Standings.id == standing.id)
            .first()
        )
        assert standing in postgres.session, 'Standing not added to database'
        assert query is not None
        assert query.id == standing.id
        assert query.user_id == standing.user_id
        assert query.course_id == standing.course_id
        postgres.session.close()

    def test_add_group(self, postgres: SQLAlchemy):
        from database import Group

        course = self.add_course(postgres)
        group = self.add_group(course, postgres)

        query = (
            postgres.session.query(Group).filter(Group.id == group.id).first()
        )
        assert group in postgres.session, 'Group not added to database'
        assert query is not None
        assert query.id == group.id
        assert query.course_id == group.course_id
        postgres.session.close()

    def test_add_user_info(self, postgres: SQLAlchemy):
        from database import UserInfo

        user = self.add_user(postgres)
        course = self.add_course(postgres)
        user_info = self.add_user_info(user, course, postgres)

        query = (
            postgres.session.query(UserInfo)
            .filter(UserInfo.id == user_info.id)
            .first()
        )
        assert user_info in postgres.session, 'UserInfo not added to database'
        assert query is not None
        assert query.id == user_info.id
        assert query.user_id == user_info.user_id
        assert query.course_id == user_info.course_id
        postgres.session.close()

    def test_group_students_exists(self, postgres: SQLAlchemy):
        course = self.add_course(postgres)
        group = self.add_group(course, postgres)
        user = self.add_user(postgres)
        self.add_group_students(user, group, postgres)

        query = postgres.session.execute(
            'SELECT * FROM group_students WHERE group_id = :group_id AND user_id = :user_id',
            {'group_id': group.id, 'user_id': user.id},
        ).fetchone()
        assert query is not None
        assert query.group_id == group.id
        assert query.user_id == user.id
        postgres.session.close()

    def test_group_tasks_exists(self, postgres: SQLAlchemy):
        user = self.add_user(postgres)
        task = self.add_task(user, postgres)
        attached_task = self.add_attached_task(task, postgres)
        course = self.add_course(postgres)
        group = self.add_group(course, postgres)
        self.add_group_tasks(group, attached_task, postgres)

        query = postgres.session.execute(
            'SELECT * FROM group_tasks WHERE group_id = :group_id AND attached_task_id = :attached_task_id',
            {'group_id': group.id, 'attached_task_id': attached_task.id},
        ).fetchone()
        assert query is not None
        assert query.group_id == group.id
        assert query.attached_task_id == attached_task.id
        postgres.session.close()

    def test_add_solution(self, postgres: SQLAlchemy):
        from database import Solution

        user = self.add_user(postgres)
        task = self.add_task(user, postgres)
        attached_task = self.add_attached_task(task, postgres)
        course = self.add_course(postgres)
        user_rater = self.add_user(postgres)
        solution = self.add_solution(
            user, attached_task, course, user_rater, postgres
        )

        query = (
            postgres.session.query(Solution)
            .filter(Solution.id == solution.id)
            .first()
        )
        assert solution in postgres.session, 'Solution not added to database'
        assert query is not None
        assert query.id == solution.id
        assert query.user_id == solution.user_id
        assert query.attached_task_id == solution.attached_task_id
        assert query.course_id == solution.course_id
        assert query.rater_id == solution.rater_id
        postgres.session.close()

    def test_add_file_metadata(self, postgres: SQLAlchemy):
        from database import FileMetadata

        file_metadata = self.add_file_metadata(postgres)

        query = (
            postgres.session.query(FileMetadata)
            .filter(FileMetadata.id == file_metadata.id)
            .first()
        )
        assert (
            file_metadata in postgres.session
        ), 'FileMetadata not added to database'
        assert query is not None
        assert query.id == file_metadata.id
        assert query.filename == file_metadata.filename
        assert query.path == file_metadata.path
        postgres.session.close()

    def test_add_solution_file(self, postgres: SQLAlchemy):
        from database import SolutionFile

        user = self.add_user(postgres)
        task = self.add_task(user, postgres)
        attached_task = self.add_attached_task(task, postgres)
        course = self.add_course(postgres)
        user_rater = self.add_user(postgres)
        solution = self.add_solution(
            user, attached_task, course, user_rater, postgres
        )
        file_metadata = self.add_file_metadata(postgres)
        solution_file = self.add_solution_file(
            solution, file_metadata, postgres
        )

        query = (
            postgres.session.query(SolutionFile)
            .filter(SolutionFile.id == solution_file.id)
            .first()
        )
        assert (
            solution_file in postgres.session
        ), 'SolutionFile not added to database'
        assert query is not None
        assert query.id == solution_file.id
        assert query.solution_id == solution.id
        assert query.file_id == file_metadata.id
        postgres.session.close()

    def test_add_task_file(self, postgres: SQLAlchemy):
        from database import TaskFile

        user = self.add_user(postgres)
        task = self.add_task(user, postgres)
        file_metadata = self.add_file_metadata(postgres)
        task_file = self.add_task_file(task, file_metadata, postgres)

        query = (
            postgres.session.query(TaskFile)
            .filter(TaskFile.id == task_file.id)
            .first()
        )
        assert task_file in postgres.session, 'TaskFile not added to database'
        assert query is not None
        assert query.id == task_file.id
        assert query.task_id == task.id
        assert query.file_id == file_metadata.id
        postgres.session.close()

    def test_add_solution_review(self, postgres: SQLAlchemy):
        from database import SolutionReview

        user = self.add_user(postgres)
        task = self.add_task(user, postgres)
        attached_task = self.add_attached_task(task, postgres)
        course = self.add_course(postgres)
        user_rater = self.add_user(postgres)
        solution = self.add_solution(
            user, attached_task, course, user_rater, postgres
        )
        solution_review = self.add_solution_review(
            solution, user_rater, postgres
        )

        query = (
            postgres.session.query(SolutionReview)
            .filter(SolutionReview.id == solution_review.id)
            .first()
        )
        assert (
            solution_review in postgres.session
        ), 'SolutionReview not added to database'
        assert query is not None
        assert query.id == solution_review.id
        assert query.solution_id == solution.id
        assert query.rater_id == user_rater.id
        postgres.session.close()
