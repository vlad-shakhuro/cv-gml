class DatabaseSetup:
    @staticmethod
    def add_user(db):
        from tests.factory_lib import UserFactory

        user = UserFactory.build()
        db.session.add(user)
        db.session.commit()
        return user

    @staticmethod
    def add_task(user, db):
        from tests.factory_lib import TaskFactory

        task = TaskFactory.build()
        task.creator_id = user.id
        db.session.add(task)
        db.session.commit()
        return task

    @staticmethod
    def add_attached_task(task, db):
        from tests.factory_lib import AttachedTaskFactory

        attached_task = AttachedTaskFactory.build()
        attached_task.task_id = task.id
        db.session.add(attached_task)
        db.session.commit()
        return attached_task

    @staticmethod
    def add_course(db):
        from tests.factory_lib import CourseFactory

        course = CourseFactory.build()
        db.session.add(course)
        db.session.commit()
        return course

    @staticmethod
    def add_user_role(user, course, db):
        from tests.factory_lib import UserRoleFactory

        user_role = UserRoleFactory.build()
        user_role.user_id = user.id
        user_role.course_id = course.id
        db.session.add(user_role)
        db.session.commit()
        return user_role

    @staticmethod
    def add_standing(user, course, db):
        from tests.factory_lib import StandingsFactory

        standing = StandingsFactory.build()
        standing.user_id = user.id
        standing.course_id = course.id
        db.session.add(standing)
        db.session.commit()
        return standing

    @staticmethod
    def add_group(course, db):
        from tests.factory_lib import GroupFactory

        group = GroupFactory.build()
        group.course_id = course.id
        db.session.add(group)
        db.session.commit()
        return group

    @staticmethod
    def add_user_info(user, course, db):
        from tests.factory_lib import UserInfoFactory

        user_info = UserInfoFactory.build()
        user_info.user_id = user.id
        user_info.course_id = course.id
        db.session.add(user_info)
        db.session.commit()
        return user_info

    @staticmethod
    def add_group_students(user, group, db):
        db.session.execute(
            'INSERT INTO group_students (group_id, user_id) VALUES (:group_id, :user_id)',
            {'group_id': group.id, 'user_id': user.id},
        )
        db.session.commit()

    @staticmethod
    def add_group_tasks(group, attached_task, db):
        db.session.execute(
            'INSERT INTO group_tasks (group_id, attached_task_id) VALUES (:group_id, :attached_task_id)',
            {'group_id': group.id, 'attached_task_id': attached_task.id},
        )
        db.session.commit()

    @staticmethod
    def add_solution(user, attached_task, course, rater, db):
        from tests.factory_lib import SolutionFactory

        solution = SolutionFactory.build()
        solution.user_id = user.id
        solution.attached_task_id = attached_task.id
        solution.course_id = course.id
        solution.rater_id = rater.id
        db.session.add(solution)
        db.session.commit()
        return solution

    @staticmethod
    def add_solution_file(solution, file, db):
        from tests.factory_lib import SolutionFileFactory

        solution_file = SolutionFileFactory.build()
        solution_file.solution_id = solution.id
        solution_file.file_id = file.id
        db.session.add(solution_file)
        db.session.commit()
        return solution_file

    @staticmethod
    def add_file_metadata(db):
        from tests.factory_lib import FileMetadataFactory

        file_metadata = FileMetadataFactory.build()
        db.session.add(file_metadata)
        db.session.commit()
        return file_metadata

    @staticmethod
    def add_task_file(task, file, db):
        from tests.factory_lib import TaskFileFactory

        task_file = TaskFileFactory.build()
        task_file.task_id = task.id
        task_file.file_id = file.id
        db.session.add(task_file)
        db.session.commit()
        return task_file

    @staticmethod
    def add_solution_review(solution, rater, db):
        from tests.factory_lib import SolutionReviewFactory

        solution_review = SolutionReviewFactory.build()
        solution_review.solution_id = solution.id
        solution_review.rater_id = rater.id
        db.session.add(solution_review)
        db.session.commit()
        return solution_review
