from enum import Enum, unique

from factory import Factory, Faker, Sequence, SubFactory

from database import (
    AttachedTask,
    Course,
    FileMetadata,
    Group,
    Solution,
    SolutionFile,
    SolutionReview,
    Standings,
    Task,
    TaskFile,
    User,
    UserInfo,
    UserRole,
)


@unique
class TaskTypes(str, Enum):
    notebook = 'notebook'
    programming = 'programming'
    test = 'test'


class UserFactory(Factory):
    class Meta:
        model = User

    id = Sequence(lambda n: n + 10)
    email = Faker('email')
    password = Faker('password')
    firstname = Faker('name')
    lastname = Faker('last_name')
    email_confirmed = True
    is_admin = True


class TaskFactory(Factory):
    class Meta:
        model = Task

    id = Sequence(lambda n: n)
    creator_id = SubFactory(UserFactory)
    type = TaskTypes.test
    name = Faker('name')
    single_solution = False
    use_peer_review = False


class AttachedTaskFactory(Factory):
    class Meta:
        model = AttachedTask

    id = Sequence(lambda n: n)
    task_id = Sequence(lambda n: n)
    start_date = Faker('date_time_this_month')
    finish_date = Faker('date_time_this_month')


class CourseFactory(Factory):
    class Meta:
        model = Course

    id = Sequence(lambda n: n)
    name = Faker('name')
    short_name = Faker('name')


class UserRoleFactory(Factory):
    class Meta:
        model = UserRole

    id = Sequence(lambda n: n)
    user_id = Sequence(lambda n: n)
    course_id = Sequence(lambda n: n)


class StandingsFactory(Factory):
    class Meta:
        model = Standings

    id = Sequence(lambda n: n)
    user_id = Sequence(lambda n: n)
    course_id = Sequence(lambda n: n)


class GroupFactory(Factory):
    class Meta:
        model = Group

    id = Sequence(lambda n: n)
    name = Faker('name')
    course_id = Sequence(lambda n: n)
    codeword = Faker('name')


class UserInfoFactory(Factory):
    class Meta:
        model = UserInfo

    id = Sequence(lambda n: n)
    info = Faker('name')
    user_id = Sequence(lambda n: n)
    course_id = Sequence(lambda n: n)


class SolutionFactory(Factory):
    class Meta:
        model = Solution

    id = Sequence(lambda n: n)
    date = Faker('date_time_this_month')
    user_id = Sequence(lambda n: n)
    attached_task_id = Sequence(lambda n: n)
    course_id = Sequence(lambda n: n)
    rater_id = Sequence(lambda n: n)

    status = 'Testing completed'


class SolutionReviewFactory(Factory):
    class Meta:
        model = SolutionReview

    id = Sequence(lambda n: n)
    rater_id = Sequence(lambda n: n)
    solution_id = Sequence(lambda n: n)


class TaskFileFactory(Factory):
    class Meta:
        model = TaskFile

    id = Sequence(lambda n: n)
    task_id = Sequence(lambda n: n)
    file_id = Sequence(lambda n: n)


class SolutionFileFactory(Factory):
    class Meta:
        model = SolutionFile

    id = Sequence(lambda n: n)
    solution_id = Sequence(lambda n: n)
    file_id = Sequence(lambda n: n)


class FileMetadataFactory(Factory):
    class Meta:
        model = FileMetadata

    id = Sequence(lambda n: n)
    filename = Faker('name')
    path = Faker('uuid4')
    hashsum = Faker('uuid4')
