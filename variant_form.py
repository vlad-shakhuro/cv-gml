from wtforms import Field
from wtforms import widgets
from wtforms.fields.html5 import DecimalField
from wtforms.utils import unset_value
import decimal


class VariantFormField(Field):
    """
    Encapsulate a form as a field in another form.
    :param form_class:
        A subclass of Form that will be encapsulated.
    :param separator:
        A string which will be suffixed to this field's name to create the
        prefix to enclosed fields. The default is fine for most uses.
    """
    widget = widgets.TableWidget()

    def __init__(self, forms, default_form_name, label=None, validators=None, separator='-', **kwargs):
        super(VariantFormField, self).__init__(label, validators, **kwargs)
        if default_form_name not in forms:
            raise TypeError('default_form_name is not found in forms')
        for name in forms:
            if separator in name:
                raise TypeError('Name "%s" contains separator ("%s")' % (name, separator))
        self.forms = forms
        self.default_form_name = default_form_name
        self.separator = separator
        self._obj = None

        if self.filters:
            raise TypeError('VariantFormField cannot take filters, as the encapsulated data is not mutable.')
        if validators:
            raise TypeError(
                'VariantFormField does not accept any validators. Instead, define them on the enclosed form.')

    def process(self, formdata, data=unset_value):
        if data is unset_value:
            try:
                data = self.default()
            except TypeError:
                data = self.default
            self._obj = data

        self.object_data = data

        prefix = self.name + self.separator
        form_name = None
        if formdata:
            offset = len(prefix)
            for k in formdata:
                if k.startswith(prefix):
                    form_name = k[offset:].split(self.separator, 1)[0]
                    break

        if data is not unset_value and data is not None:
            if isinstance(data, dict):
                form_name = data['type']
            else:
                form_name = data.type

        if form_name is None:
            form_name = self.default_form_name

        if form_name not in self.forms:
            raise TypeError('Form name "%s" is not found' % form_name)
        self.form_name = form_name
        form_class = self.forms[form_name]

        prefix_ = prefix + form_name + self.separator

        if isinstance(data, dict):
            self.form = form_class(formdata=formdata, prefix=prefix_, **data)
        else:
            self.form = form_class(formdata=formdata, obj=data, prefix=prefix_)

        inst_forms = {}
        for form_name, form_class in self.forms.items():
            prefix_ = prefix + form_name + self.separator
            inst_forms[form_name] = form_class(prefix=prefix_)
        self.inst_forms = inst_forms

    def validate(self, form, extra_validators=tuple()):
        if extra_validators:
            raise TypeError(
                'VariantFormField does not accept in-line validators, as it gets errors from the enclosed form.')
        return self.form.validate()

    def populate_obj(self, obj, name):
        candidate = getattr(obj, name, None)
        if candidate is None:
            if self._obj is None:
                raise TypeError(
                    'populate_obj: cannot find a value to populate from the provided obj or input data/defaults')
            candidate = self._obj
            setattr(obj, name, candidate)

        self.form.populate_obj(candidate)

    def __iter__(self):
        return iter(self.form)

    def __getitem__(self, name):
        return self.form[name]

    def __getattr__(self, name):
        return getattr(self.form, name)

    @property
    def data(self):
        if 'type' in self.form.data:
            raise TypeError('formdata already contains "type" field, can not append auxiliary info')
        data = {'type': self.form_name}
        data.update(self.form.data)
        return data

    @property
    def errors(self):
        return self.form.errors

    @property
    def tpl_forms(self):
        return self.inst_forms


class DecimalFieldRu(DecimalField):
    def process_formdata(self, valuelist):
        if valuelist:
            try:
                if self.use_locale:
                    self.data = self._parse_decimal(valuelist[0])
                else:
                    self.data = decimal.Decimal(valuelist[0])
            except (decimal.InvalidOperation, ValueError):
                self.data = None
                raise ValueError('Требуется ввести десятичное число')
