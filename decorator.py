from functools import wraps
from flask import redirect, request, render_template, url_for
from flask_login import current_user
from datetime import datetime


def templated(template=None):
    def decorator(f):
        @wraps(f)
        def decorated_function(*args, **kwargs):
            template_name = template
            if template_name is None:
                template_name = request.endpoint \
                    .replace('.', '/') + '.html'
            ctx = f(*args, **kwargs)
            if ctx is None:
                ctx = {}
            elif not isinstance(ctx, dict):
                return ctx
            if 'template' in ctx:
                template_name = ctx['template']
            if 'now' not in ctx:
                ctx['now'] = datetime.now()
            return render_template(template_name, **ctx)
        return decorated_function
    return decorator


def auth_user(f):
    @wraps(f)
    def decorated(*args, **kwargs):
        if not current_user.is_authenticated:
            return redirect(url_for('login'))
        return f(*args, **kwargs)
    return decorated
