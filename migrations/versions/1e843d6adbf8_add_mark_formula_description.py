"""Add mark formula description

Revision ID: 1e843d6adbf8
Revises: e227c2335ad0
Create Date: 2019-12-16 13:25:32.389791

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '1e843d6adbf8'
down_revision = 'e227c2335ad0'
branch_labels = None
depends_on = None


def upgrade():
    op.add_column('attached_task', sa.Column('mark_formula_description', sa.String(length=1024), nullable=True))


def downgrade():
    op.drop_column('attached_task', 'mark_formula_description')
