"""Add codeword end date

Revision ID: 8c18af438e56
Revises: af39d5bce1c1
Create Date: 2019-12-13 18:05:38.176598

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '8c18af438e56'
down_revision = 'af39d5bce1c1'
branch_labels = None
depends_on = None


def upgrade():
    op.add_column('group', sa.Column('codeword_end_date', sa.DateTime(), nullable=True))


def downgrade():
    op.drop_column('group', 'codeword_end_date')
