"""Add mark formula version

Revision ID: af39d5bce1c1
Revises:
Create Date: 2019-12-13 16:12:07.078345

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = 'af39d5bce1c1'
down_revision = None
branch_labels = None
depends_on = None


def upgrade():
    op.add_column('attached_task', sa.Column('mark_formula_version', sa.String(length=2), server_default='v1'))
    op.execute('UPDATE attached_task SET mark_formula_version=\'v1\'')
    op.alter_column('attached_task', 'mark_formula_version', nullable=False)


def downgrade():
    op.drop_column('attached_task', 'mark_formula_version')
