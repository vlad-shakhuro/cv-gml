"""Del mark formula version

Revision ID: 76baf54c96d4
Revises: 1e843d6adbf8
Create Date: 2019-12-27 16:26:28.712881

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '76baf54c96d4'
down_revision = '1e843d6adbf8'
branch_labels = None
depends_on = None


def upgrade():
    op.drop_column('attached_task', 'mark_formula_version')


def downgrade():
    op.add_column('attached_task', sa.Column('mark_formula_version', sa.VARCHAR(length=2), server_default=sa.text("'v1'::character varying"), autoincrement=False, nullable=False))
